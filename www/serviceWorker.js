const sw = 'elyzer-v1'
const assets = [
	'/index.html',
	'https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.2/vue.js',
	'/app.js',
	'/menuButton/menuButton.js',
	'/app.css',
	'/colors.css',
	'/menuButton/menuButton.css'
	'/res/icon_192.png',
	'/res/icon_512.png',
	'https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0',
	'https://fonts.googleapis.com/css2?family=Montserrat:wght@100;400;600&display=swap'
];

self.addEventListener('install', installEvent => {
	self.skipWaiting();
	installEvent.waitUntil(
		caches.open(sw).then(cache => {
			cache.addAll(assets);
		})
	);
});

self.addEventListener("fetch", fetchEvent => {
	fetchEvent.respondWith(
		caches.match(fetchEvent.request).then(res => {
			return res || fetch(fetchEvent.request);
		})
	);
});