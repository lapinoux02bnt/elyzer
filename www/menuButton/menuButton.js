Vue.component('menu-button', {
	template: `<div class="control">
		<div class="winners-menu" :class="{open: open}">
			<div v-for="option in options" class="menu-item" :class="{selected: value === option}" @touchstart.stop.prevent="select(option)">{{option}}<div v-if="value === option" class="selector"></div></div>
		</div>
		<div class="material-symbols-outlined winners-button" @touchstart.stop.prevent="toggle">{{icon}}</div>
		<div v-if="selected" class="menu-button-selected"></div>
	</div>`,
	props: ['value', 'options', 'icon', 'selected'],
	data() {
		return {
			open: false
		}
	},
	methods: {
		openMenu() {
			this.open = true
			this.$emit('open')
		},
		closeMenu() {
			this.open = false
		},
		toggle() {
			if (this.open) {
				this.closeMenu()
			} else {
				this.openMenu()
			}
		},
		select(option) {
			this.$emit('input', option)
		}
	}
})