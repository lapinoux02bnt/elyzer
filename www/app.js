let timer = 3000

if ('serviceWorker' in navigator) {
	window.addEventListener('load', () => {
		navigator.serviceWorker.register('/serviceWorker.js')
	})
}

new Vue({
	el: '#app',
	data() {
		let colors = ['#bdff00', '#00ffa3', '#ff9900', '#bd00ff', '#ff0000', '#ff0099']
		let colorNames = colors.map((color, i) => {
			document.documentElement.style.setProperty(`--color-${i}`, color) // generates css with the colors, used in colors.css
			return `color-${i}`
		})
		return {
			colors: colorNames,
			touchPoints: [],
			touchEventTimeoutId: undefined,
			touchEventReceived: false,
			hasChoice: false,
			hasTeam: false,
			nbWinners: 1,
			teams: 2,
			mode: 1 // 1: winner, 2: teams
		}
	},
	mounted() {
		this.shuffleColors()
	},
	computed: {
		finishScreen() {
			return this.hasChoice || this.hasTeam
		},
		deviceHasTouch() {
			return this.touchEventReceived || "ontouchstart" in document.documentElement;
		}
	},
	methods: {
		handleTouch(e) {
			if (this.finishScreen) return
			this.touchEventReceived = true;
			[...e.changedTouches].forEach(touch => {
				this.touchPoints.push({
					id: touch.identifier,
					color: this.mode === 1 ? this.colors[touch.identifier % this.colors.length] : 'color-white',
					style: {left: touch.clientX + 'px', top: touch.clientY + 'px'}
				})
			})
			this.rearmTimeout()
		},
		handleMove(e) {
			[...e.changedTouches].forEach(touch => {
				let touchPoint = this.touchPoints.find(d => d.id == touch.identifier)
				touchPoint.style.left = touch.clientX + 'px'
				touchPoint.style.top = touch.clientY + 'px'
			})
		},
		handleStop(e) {
      let touchIdentifiers = [...e.changedTouches].map(touch => touch.identifier)
      this.touchPoints = this.touchPoints.filter((d) => !touchIdentifiers.includes(d.id))
			if (!this.touchPoints.length) {
				this.reset()
				return
			} else if (!this.finishScreen) {
				this.rearmTimeout()
			}
		},
		rearmTimeout() {
			clearTimeout(this.touchEventTimeoutId);
			if (this.mode === 1 && this.touchPoints.length > this.nbWinners || this.mode === 2 && this.touchPoints.length >= this.teams) {
				this.touchEventTimeoutId = setTimeout(this.choose, timer);
			}
		},
		choose() {
			if (this.mode === 1) {
				this.chooseWinners()
			} else if (this.mode === 2) {
				this.chooseTeams()
			}
		},
		chooseWinners() {
			let chosen = this.getDistinct(this.touchPoints, this.nbWinners)
			chosen.forEach(c => c.chosen = true)
			this.hasChoice = true
		},
		chooseTeams() {
			let teamsColors = this.colors.slice(0, this.teams)
			let touchPoints = [...this.touchPoints]
			let i = 0
			while (touchPoints.length) {
				let tp = touchPoints.splice(Math.floor(Math.random() * touchPoints.length), 1)[0]
				tp.color = teamsColors[(++i) % this.teams]
				tp.teamed = true
			}
			this.hasTeam = true
		},
		reset() {
			clearTimeout(this.touchEventTimeoutId);
			this.touchEventTimeoutId = undefined
			this.hasChoice = false
			this.hasTeam = false
			this.shuffleColors()
		},
		openWinners() {
			this.mode = 1
			this.$refs.teamButton.closeMenu()
		},
		openTeams() {
			this.mode = 2
			this.$refs.winnerButton.closeMenu()
		},
		getDistinct(collection, number) {
			if (number > collection.length) return [...collection]
			let col = [...collection]
			let res = []
			for (let i = 0; i < number; ++i) {
				res.push(...col.splice(Math.floor(Math.random() * col.length), 1))
			}
			return res
		},
		shuffleColors() {
			// knuth suffle implementation
			for (let i = this.colors.length - 1; i > 0 ; --i) {
				let j = Math.floor(Math.random() * (i + 1));
				[this.colors[i], this.colors[j]] = [this.colors[j], this.colors[i]]
			}
		}
	}
})
